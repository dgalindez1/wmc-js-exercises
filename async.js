const exercises = [];

exercises.push({
  noOutput: true,
  description: 'Ejercicio 1: Recorre un arreglo usando un callback, imprime cada elemento',
  execute: () => {
    [1, 2, 3, 'a', 'b', { c: true }].forEach(v => console.log(v));
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 2: Varios callbacks, imprime 1, 2 y 3 en 3, 1 y 4 segundos respectivamente',
  execute: () => {
    console.log('Inicio');
    setTimeout(console.log, 3000, 1);
    setTimeout(console.log, 1000, 2);
    setTimeout(console.log, 4000, 3);
    setTimeout(console.log, 5000, 'Fin de ejecucion');
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 3: Escribe un callback que recibe un argumento y lo imprime',
  execute: () => {
    function parameterizedCallback(value) {
      console.log(value);
    }
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 4: Crea una función que recibe el callback anterior y lo ejecuta',
  execute: () => {
    function executeCallback(callback, value) {
      callback(value);
    }
    executeCallback(parameterizedCallback, 'someValue');
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 5: Crea una funcion que llama a un callback y usa el valor que regresa para llamar a otro. Despues debe llamar a un tercer callback con el valor que regresa el segundo',
  execute: () => {
    function firstCallback(callback) {
      callback('Data from first CB');
    }
    function secondCallback(first, callback) {
      callback(`Data from second CB, ${first}`);
    }
    function thirdCallback(first, second, callback) {
      callback(`Data from third CB, ${second}, ${first}`);
    }

    firstCallback(first => {
      secondCallback(first, second => {
        thirdCallback(first, second, third => {
          console.log(third);
        })
      })
    });
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 6: Convertir 1, 2, 5 en Promises',
  execute: () => {
    console.log('1');
    [1, 2, 3, 'a', 'b', { c: true }].forEach((v) => Promise.resolve().then(() => console.log(v)));
    console.log('2');
    console.log('Inicio');
    new Promise((resolve, reject) => setTimeout(resolve, 3000, 1)).then((v) => console.log(v));
    new Promise((resolve, reject) => setTimeout(resolve, 1000, 2)).then((v) => console.log(v));
    new Promise((resolve, reject) => setTimeout(resolve, 4000, 3)).then((v) => console.log(v));
    new Promise((resolve, reject) => setTimeout(resolve, 5000, 'Fin de ejecucion')).then((v) => console.log(v));

    console.log('5');
    Promise.resolve('Data from first Promise').then(first => {
      return Promise.resolve(`Data from second Promise, ${first}`).then(second => {
        return Promise.resolve(`Data from third Promise, ${second}, ${first}`).then(third => {
          console.log(third);
        });
      });
    })
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 7: Crea una cadena de promesas que lanza un error (sin hacer Catch)',
  execute: () => {
    Promise.resolve(1).then(v => {throw new Error('ahhhh!!!')});
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 8: Agrega un catch al ejercicio anterior',
  execute: () => {
    Promise.resolve(1).then(v => { throw new Error('ahhhh!!!') }).catch(console.log);
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 9: Agrega un finally al ejercicio anterior',
  execute: () => {
    Promise.resolve(1).then(v => { throw new Error('ahhhh!!!') }).catch(console.log).finally(() => console.log('Done!'));
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 10: Crea una funcion con 3 promesas, que acaben a tiempos diferentes y regresa en cuanto la primera termina',
  execute: () => {
    function race() {
      let p1 = new Promise((resolve, reject) => setTimeout(resolve, 1000, 1));
      let p2 = new Promise((resolve, reject) => setTimeout(resolve, 2000, 2));
      let p3 = new Promise((resolve, reject) => setTimeout(resolve, 3000, 3));
      return Promise.race([p1, p2, p3]);
    }
    race().then(v => console.log(`Got ${v}`));
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 11: Crea una funcion con 4 promesas, que acaben a tiempos diferentes y regresa cuando todas acaben',
  execute: () => {
    function all() {
      let p1 = new Promise((resolve, reject) => setTimeout(resolve, 1000, 1));
      let p2 = new Promise((resolve, reject) => setTimeout(resolve, 2000, 2));
      let p3 = new Promise((resolve, reject) => setTimeout(resolve, 3000, 3));
      let p4 = new Promise((resolve, reject) => setTimeout(resolve, 100, 4));
      return Promise.all([p1, p2, p3, p4]);
    }
    all().then(arr => console.log(`Got ${arr}`));
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 12: Crea una cadena de promesas, con una rechazada y un catch',
  execute: () => {
    Promise.resolve(1).then(v => v + v).then(v => { throw new Error(v) }).then(v => v + 8).catch(console.log);
  }
});

exercises.push({
  noOutput: true,
  description: 'Ejercicio 13: Crea una cadena de promesas, con una rechazada y varios catch',
  execute: () => {
    Promise.resolve(42).then(v => { throw new Error(v) }).catch(console.log).then(() => 0)
      .catch(console.log).catch(console.log);
  }
});

/* Explica la salida de las siguientes funciones
// 4, el Reject se salta los thens siguientes hasta el catch
Promise.resolve(1).then(v => v + 3).then(v => Promise.reject(v))
  .then(v => v * 3).catch(e => console.log(e));
// 5, el reject inicial hace que entre directo al catch, los thens que le siguen se ejecutan normalmente
// Catch regresa una promesa resuelta
Promise.reject(2).then(v => v + 3).catch(e => e * 3).then(v => v - 1).then(v => console.log(v));
// 3, con un rechazo regresa una promesa rechazada, que va directo al catch
Promise.all([Promise.resolve(1), Promise.resolve(2), Promise.reject(3), Promise.resolve(1)])
  .then(arr => console.log(arr)).catch(e => console.log(e));
// 1, en el then, ya que resuelven de inmediato, se ejecutan en el orden que se enviaron al metodo
// Con una que resuelva o rechace, race termina la ejecucion
Promise.race([Promise.resolve(1), Promise.reject(2), Promise.reject(3), Promise.resolve(1)])
  .then(v => console.log(v)).catch(e => console.log(e));
// 1, en el catch, ya que rechaza de inmediato
Promise.race([Promise.reject(1), Promise.resolve(2), Promise.reject(3), Promise.resolve(1)])
  .then(v => console.log(v)).catch(e => console.log(e));
// [1, 2, 3, 1], en el then, todas resuelven
Promise.all([Promise.resolve(1), Promise.resolve(2), Promise.resolve(3), Promise.resolve(1)])
  .then(arr => console.log(arr)).catch(arr => console.log(arr));
*/

exercises.push({
  noOutput: true,
  description: 'Ejercicio 14: Convierte el ejercicio 5 a async/await',
  execute: () => {
    function firstCallback() {
      return Promise.resolve('Data from first CB');
    }
    function secondCallback(first) {
      return Promise.resolve(`Data from second CB, ${first}`);
    }
    function thirdCallback(first, second) {
      return Promise.resolve(`Data from third CB, ${second}, ${first}`);
    }

    async function execute() {
      const first = await firstCallback();
      const second = await secondCallback(first);
      const third = await thirdCallback(first, second);
      console.log(third);
    }

    execute();
  }
});