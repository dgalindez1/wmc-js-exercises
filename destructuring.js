// Para todos los ejercicios con listeners que sean posibles, usa destructuring
window.onload = () => {
  document.querySelector('#div1').innerHTML = 'I am a div!';
  document.querySelector('#div2 .a-span').innerHTML = 'I am a span!';
  document.querySelector('#div3 .a-button').addEventListener('click', () => alert('I was clicked'));
  document.querySelector('#div4 .a-button').addEventListener('click', (event) => {
    event.preventDefault();
    alert('Default prevented');
  });
  document.querySelector('#div5').addEventListener('click', ({target}) => alert(target));
  document.querySelector('#div6').addEventListener('click', ({target}) => alert(`Div listener: ${target}`));
  document.querySelector('#div6 .a-span').addEventListener('click', ({target}) => alert(`Span listener: ${target}`));
  document.querySelector('#div7').addEventListener('click', ({target}) => alert(`Div listener: ${target}`));
  document.querySelector('#div7 .a-span').addEventListener('click', (event) => {
    event.stopPropagation();
    alert(`Span listener: ${event.target}`);
  });
  document.querySelectorAll('#div8 .a-span').forEach((s, i) => s.textContent = i + 1);
  document.querySelector('#div9 .a-span').addEventListener('click', () => alert('First span!'));
  document.querySelector('#div9 .a-span:last-of-type').addEventListener('click', () => alert('Last span!'));
  document.querySelectorAll('#div9 .a-span:nth-of-type(odd)').forEach((s, i) => s.addEventListener('click', () => alert(`Span ${i + 1}`)));
  const notASpan11 = document.createElement('span');
  notASpan11.className = 'not-a-span';
  notASpan11.innerText = 'Not a span!';
  document.querySelector('#div11').appendChild(notASpan11);
  document.querySelector('#div12 img').setAttribute('src', 'dogge.jpg');
  document.querySelector('#div13 span').setAttribute('id', 'THE-span');
  document.querySelector('#div14 input[name="changeInput"').addEventListener('change', ({ target: { name }}) => alert(name));
  document.querySelector('#div14 input[name="blurInput"').addEventListener('blur', ({ target: { name }}) => alert(name));
  document.querySelector('#div14 input[name="bothInput"').addEventListener('change', ({ target: { name }}) => alert(name));
  document.querySelector('#div14 input[name="bothInput"').addEventListener('blur', ({ target: { name }}) => alert(name));
  let span15OriginalText = '';
  document.querySelector('#div15 .a-span').addEventListener('mouseenter', ({target}) => {
    span15OriginalText = target.innerText;
    target.innerText = 'I am being hovered';
  });
  document.querySelector('#div15 .a-span').addEventListener('mouseleave', ({target}) => {
    target.innerText = span15OriginalText;
  });
  document.querySelector('#div16').addEventListener('scroll', () => console.log('I was scrolled!'));
};
