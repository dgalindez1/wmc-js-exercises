// Para todos los ejercicios, si no se especifica que se espera que haga un listener, asuman que debe imprimir algo
window.onload = () => {
  // Agrega contenido a div1 (ejemplo: 'I am a div!')
  document.querySelector('#div1').innerHTML = 'I am a div!';
  // Agrega contenido al span de div2 (ejemplo: 'I am a span!')
  document.querySelector('#div2 .a-span').innerHTML = 'I am a span!';
  // Agrega un listener de click al boton de div3, el listener debe imprimir algo (ejemplo: 'I was clicked')
  document.querySelector('#div3 .a-button').addEventListener('click', () => alert('I was clicked'));
  // Agrega un listener de click al boton de div4, no dejes que haga el submit e imprime 'Default prevented'
  document.querySelector('#div4 .a-button').addEventListener('click', (event) => {
    event.preventDefault();
    alert('Default prevented');
  });
  // Agrega un listener de click a div5, imprime el target del evento
  document.querySelector('#div5').addEventListener('click', (event) => alert(event.target));
  // Agrega un listener de click al span de div6 y a la div6, imprime el target del evento en ambos y si es el listener de div o de span
  document.querySelector('#div6').addEventListener('click', (event) => alert(`Div listener: ${event.target}`));
  document.querySelector('#div6 .a-span').addEventListener('click', (event) => alert(`Span listener: ${event.target}`));
  // Agrega un listener de click al span de div7 y a la div7, imprime el target del evento en ambos y si es el listener de div o de span, evita que el click del span se propague
  document.querySelector('#div7').addEventListener('click', (event) => alert(`Div listener: ${event.target}`));
  document.querySelector('#div7 .a-span').addEventListener('click', (event) => {
    event.stopPropagation();
    alert(`Span listener: ${event.target}`);
  });
  // Selecciona los spans de div8, agrega el numero que son como contenido a cada una (1-6)
  document.querySelectorAll('#div8 .a-span').forEach((s, i) => s.textContent = i + 1);
  // Agrega un listener de click al primer y ultimo span de div9
  document.querySelector('#div9 .a-span').addEventListener('click', () => alert('First span!'));
  document.querySelector('#div9 .a-span:last-of-type').addEventListener('click', () => alert('Last span!'));
  // Agrega un listener de click a los spans impares de div10 (1, 3, 5)
  document.querySelectorAll('#div9 .a-span:nth-of-type(odd)').forEach((s, i) => s.addEventListener('click', () => alert(`Span ${i + 1}`)));
  // Agrega otro span a div11, con 'not-a-span' como clase y 'Not a span!' como contenido
  const notASpan11 = document.createElement('span');
  notASpan11.className = 'not-a-span';
  notASpan11.innerText = 'Not a span!';
  document.querySelector('#div11').appendChild(notASpan11);
  // Cambia el src de la imagen de div12 a dogge.jpg (adjunto)
  document.querySelector('#div12 img').setAttribute('src', 'dogge.jpg');
  // Pon 'THE-span' como id del span de div13
  document.querySelector('#div13 span').setAttribute('id', 'THE-span');
  /**
   * Agrega listeners a los 3 inputs de div14.
   * change para changeInput
   * blur para blurInput
   * ambos para bothInput
   *
   * Los listeners deben imprimir el nombre del target del evento
   */
  document.querySelector('#div14 input[name="changeInput"').addEventListener('change', (event) => alert(event.target.name));
  document.querySelector('#div14 input[name="blurInput"').addEventListener('blur', (event) => alert(event.target.name));
  document.querySelector('#div14 input[name="bothInput"').addEventListener('change', (event) => alert(event.target.name));
  document.querySelector('#div14 input[name="bothInput"').addEventListener('blur', (event) => alert(event.target.name));
  /**
   * Agrega un listener al span de div15 que, cuando el mouse pase por encima,
   * cambie el texto a 'I am being hovered'.
   * Debe volver al texto original cuando el mouse deje de estar encima.
   */
  let span15OriginalText = '';
  document.querySelector('#div15 .a-span').addEventListener('mouseenter', (event) => {
    span15OriginalText = event.target.innerText;
    event.target.innerText = 'I am being hovered';
  });
  document.querySelector('#div15 .a-span').addEventListener('mouseleave', (event) => {
    event.target.innerText = span15OriginalText;
  });
  /**
   * Agrega un listener de scroll a la div16.
   * Debe imprimir algo ('I was scrolled') cada vez que se hace scroll de ella.
   * Se recomienda NO usar alert para este ejercicio.
   */
  document.querySelector('#div16').addEventListener('scroll', () => console.log('I was scrolled!'));
};
