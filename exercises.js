const exercises = [];

exercises.push({
  description: 'Imprime la fecha actual',
  execute: () => new Date()
});

exercises.push({
  description: 'Obten el área de un triángulo. Pidele los 3 lados al usuario',
  input: () => createPrompts(numberTimesInput(3)),
  execute: (sides) => {
    const p = sides.reduce((a, b) => a + b) / 2;
    return Math.sqrt(sides.reduce((a, b) => a * (p - b), p));
  }
});

exercises.push({
  description: 'Voltea una string dada por el usuario',
  input: () => createPrompts(textSingleInput),
  execute: (text) => text.split('').reverse().join('')
});

exercises.push({
  description: 'Voltea una string dada por el usuario sin usar el método de reverse',
  input: () => createPrompts(textSingleInput),
  execute: (text) => text.split('').map((_, i, a) => a[a.length - i - 1]).join('')
});

exercises.push({
  description: 'Convierte una temperatura dada en C o F a F o C, respectivamente',
  input: () => createPrompts(textSingleInput),
  execute: (temperature) => {
    const degrees = parseFloat(temperature.substring(0, temperature.length - 1));
    return temperature.endsWith('F') ? (degrees - 32) * 5 / 9 : degrees * 9 / 5 + 32;
  }
});

exercises.push({
  description: 'Crea un objeto que tenga una propiedad cuyo nombre es definido por el usuario. Su valor debe ser true',
  input: () => createPrompts(textSingleInput),
  execute: (name) => ({ [name]: true })
});

exercises.push({
  description: 'Realiza la suma de los valores que de el usuario hasta recibir un valor negativo',
  input: () => createPrompts(numberArrayInput),
  execute: (values) => values.reduce((a, b) => a + b)
});

exercises.push({
  description: 'Convierte a mayúscula la primer letra de cada palabra en una cadena dada por el usuario',
  input: () => createPrompts(textSingleInput),
  execute: (sentence) =>
    sentence
      .split(' ')
      .map((word) => word.charAt(0).toUpperCase() + word.substring(1))
      .join(' ')
});

exercises.push({
  description: 'Revisa si un número dado es múltiplo de 3 o de 7',
  input: () => createPrompts(numberSingleInput),
  execute: (number) => `Multiplo de ${!(number % 7) ? '7, ' : ''}${!(number % 3) ? '3' : ''}`
});

exercises.push({
  description: 'Revisa cuantas veces se repite un caracter dado en una cadena dada',
  input: () => createPrompts(textTimesInput(2)),
  execute: ([text, character]) => text.split(character).length - 1
});

exercises.push({
  description: 'Revisa cuantas veces se repite un valor dado en un arreglo',
  input: () => {
    return [
      createPrompts(textArrayInput),
      createPrompts(textSingleInput),
    ]
  },
  execute: ([values, value]) => values.filter((current) => current === value).length
});

exercises.push({
  description: 'Divide un número de 3 dígitos en sus centenas, decenas y unidades',
  input: () => createPrompts(numberSingleInput),
  execute: (number) => {
    const numberString = number.toString();
    let end = numberString.length - 2;
    const hundreds = numberString.substring(0, end);
    const tens = numberString.charAt(end++);
    const ones = numberString.charAt(end++);
    return `Centenas: ${hundreds}\nDecenas: ${tens}\nUnidades: ${ones}`;
  }
});

exercises.push({
  description: 'Regresa todos los caracteres que no sean letras de una cadena',
  input: () => createPrompts(textSingleInput),
  execute: (text) => text.split(/[A-Za-z]/).join('').split('')
});

exercises.push({
  description: `Haz una función que lanza un error con el mensaje dado por el usuario\n'Extiende la función anterior para atrapar el error e imprimir su mensaje y stack,`,
  input: () => createPrompts(textSingleInput),
  execute: (message) => {
    const errorThrown = () => { throw new Error(message) }
    try {
      errorThrown();
    } catch (error) {
      return `Message: ${error.message}\nStack: ${error.stack}`;
    }
  }
});

exercises.push({
  description: 'Suma los contenidos de un arreglo de números',
  input: () => createPrompts(numberArrayInput),
  execute: (numbers) => numbers.reduce((a, b) => a + b)
});

exercises.push({
  description: 'Regresa un arreglo nuevo con el cuadrado de cada valor del arreglo original',
  input: () => createPrompts(numberArrayInput),
  execute: (numbers) => numbers.map((n) => n * n)
});

exercises.push({
  description: 'Regresa que tipo de ángulo es el dado',
  input: () => createPrompts(numberSingleInput),
  execute: (angle) => {
    let val = Math.abs(angle);
    val = val > 360 ? ((val / 360) % 1) * 360 : val;
    val = val > 180 ? 360 - val : val;
    if (val < 90) return 'Agudo';
    if (val === 90) return 'Recto';
    if (val < 180) return 'Obtuso';
    if (val === 180) return 'Llano';
  }
});

exercises.push({
  description: 'Regresa un arreglo nuevo sin valores repetidos de un arreglo original',
  input: () => createPrompts(textArrayInput),
  execute: (values) => Array.from(new Set(values))
});

exercises.push({
  description: 'Quita los valores repetidos de un arreglo (sin usar otro arreglo)',
  input: () => createPrompts(textArrayInput),
  execute: (values) => {
    for (let current = 0; current <= values.length; current++) {
      let value = values[current];
      let repeat = false;
      let index = values.indexOf(value);
      while (index >= 0) {
        index = values.indexOf(value, current + 1);
        if (index >= 0) {
          repeat = true;
          values.splice(index, 1);
        }
      }
      if (repeat) {
        values.splice(current, 1);
        current--;
      }
    };
    return values;
  }
});

exercises.push({
  description: 'Convierte un número binario dado por el usuario a decimal',
  input: () => createPrompts(numberSingleInput),
  execute: (number) => parseInt(number, 2)
});

exercises.push({
  description: 'Convierte un número decimal dado por el usuario a binario, octal y hexadecimal (bases 2, 8 y 16)',
  input: () => createPrompts(numberSingleInput),
  execute: (number) => [number.toString(2), number.toString(8), number.toString(16)]
});

exercises.push({
  description: 'Regresa la cantidad de valores que comparten dos arreglos diferentes',
  input: () => {
    return [
      createPrompts(textArrayInput),
      createPrompts(textArrayInput),
    ];
  },
  execute: ([A, B]) => A.filter((v) => B.includes(v)).length
});

exercises.push({
  description: 'Valida que una cadena dada no tenga espacios en blanco',
  input: () => createPrompts(textSingleInput),
  execute: (text) => !(/\W/g.test(text))
});

exercises.push({
  description: 'Dada una cadena, determina su valor de scrabble',
  input: () => createPrompts(textSingleInput),
  execute: (text) => {
    const values = {
      A: 1, E: 1, I: 1, O: 1, N: 1, R: 1, T: 1, L: 1, S: 1, U: 1,
      D: 2, G: 2,
      B: 3, C: 3, M: 3, P: 3,
      F: 4, H: 4, V: 4, W: 4, Y: 4,
      K: 5,
      J: 8, X: 8,
      Q: 10, Z: 10
    };
    return text.split('').reduce((a, b) => a + values[b.toUpperCase()], 0);
  }
});

exercises.push({
  description: 'Determina si una palabra dada por el usuario es un palindromo',
  input: () => createPrompts(textSingleInput),
  execute: (text) => text === text.split('').reverse().join('')
});

exercises.push({
  description: 'Obten el Máximo común divisor de dos números dados',
  input: () => createPrompts(numberTimesInput(2)),
  execute: ([a, b]) => {
    let gcd = (a, b) => !b ? a : gcd(b, a % b);
    return gcd(a, b);
  }
});

exercises.push({
  description: 'Implementa una lista ligada',
  execute: () => {
    let list = {
      current: 0,
      values: [],
      add: (v) => list.values.push(v),
      next: () => {
        list.current = list.current + 1 === list.values.length ? 0 : list.current + 1;
        return list.values[list.current];
      },
      delete: () => list.values.splice(list.current, 1),
    }
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(4);
    console.log('Initial:', [1, 2, 3, 4]);
    console.log('Next:', list.next());
    console.log('Next:', list.next());
    console.log('Next:', list.next());
    console.log('Next:', list.next());
    console.log('Next:', list.next());
    console.log('Delete:', list.delete());
    console.log('Next:', list.next());
    return 'Done!';
  }
});

exercises.push({
  description: 'Implementa una lista doblemente ligada',
  execute: () => {
    let list = {
      current: 0,
      values: [],
      add: (v) => list.values.push(v),
      next: () => {
        list.current = Math.min(list.current + 1, list.values.length - 1);
        return list.values[list.current];
      },
      previous: () => {
        list.current = Math.max(list.current - 1, 0);
        return list.values[list.current];
      },
      delete: () => list.values.splice(list.current, 1),
    }
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(4);
    console.log('Initial:', [1, 2, 3, 4]);
    console.log('Next:', list.next());
    console.log('Previous:', list.previous());
    console.log('Previous:', list.previous());
    console.log('Next:', list.next());
    console.log('Next:', list.next());
    console.log('Next:', list.next());
    console.log('Next:', list.next());
    console.log('Delete:', list.delete());
    console.log('Next:', list.next());
    return 'Done!';
  }
});