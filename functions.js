const exercises = [];

exercises.push({
  description: 'Escribe una función llamada calculateDogAge que reciba un número (la edad humana del perro) y calcule su edad en años de perro (edad x 7)',
  input: () => createPrompts(numberSingleInput),
  execute: (age) => age * 7
});

exercises.push({
  description: 'Escribe una función llamada getCandySupply que reciba dos números (edad, cantidad por día) y calcule cuantos dulces consumirías por el resto de tu vida.',
  input: () => createPrompts(numberTimesInput(2)),
  execute: ([age, consumption]) =>  consumption * 365 * (75 - age)
});

exercises.push({
  description: 'Crea una función que permita encadenar llamadas',
  execute: () => {
    let value = 1;
    const foo = () => {
      value++;
      console.log(value);
      return {foo};
    }
    foo().foo().foo();
    return 'Done!'
  }
});

exercises.push({
  description: 'Crea una función que recibe un tipo (formal/casual) y dos funciones, una para un saludo casual (hola!) y otra para un saludo formal (buenos dias)',
  input: () => ([createPrompts(textSingleInput), () => 'Hola!', () => 'Buenos dias']),
  execute: ([type, casual, formal]) => {
    if (type === 'formal') {
      return formal();
    }
    return casual();
  }
});

// Conviertan cualquier ciclo for que use un arreglo de la parte 1 para que use forEach o map
// Explica cual es la salida de las siguientes funciones y porqué:
/*
// Salida: 12, solo se define y asigna una vez
var a = 12;
(function() {
  alert(a);
})();
*/

/*
// Salida: 12, usa el valor del scope local
var a = 5;
(function() {
  var a = 12;
  alert(a);
})();
*/

/*
// Salida: 12 solo una vez ya que x es una IIFE, usa el valor del scope local, es una closure
var a = 10;
var x = (function() {
  var a = 12;
  return (function() {
    alert(a);
  });
})();
x();
*/

/*
// Salida: 10 solo una vez, usa el valor del scope global, ya que 'y' no es ejecutado y tiene su propio scope aun si se ejecuta
var a = 10;
var x = (function() {
  var y = function() {
    var a = 12;
  };
  return function() {
    alert(a);
  }
})();
x();
*/

/*
// Salida: 12, usa el valor de la IIFE interna (a = 12). Como no redeclara, funciona como closure (usa el scope de fuera)
var a = 10;
var x = (function() {
  (function() {
    a = 12;
  })();
  return (function() {
    alert(a);
  });
})();
x();
*/

/*
// Salida: 15, usa el scope local, aunque x pertenezca a window, porque es una closure
var a = 10;
(function() {
  var a = 15;
  window.x = function() {
    alert(a);
  }
})();
x();
*/

/*
// Salida: undefined y luego 2, undefined por hoisting en a y 2 por el valor de foo
function leFunction() {
  console.log(a);
  console.log(foo());

  var a = 1;
  function foo() {
     return 2;
  }
}
leFunction();
*/

/*
// Salida: firstResult tiene una funcion, result tiene 5 ya que regresa a despues de ejecutar someFunction
var a = 1;
function someFunction(number) {
  function otherFunction(input) {
    return a;
  }
  a = 5;
  return otherFunction;
}

var firstResult = someFunction(9);
var result = firstResult(2);
*/

/*
// Salida: Le Name, AName y Full Name. El primero porque toma el de prop (this en getFullname se refiere a prop)
//  El segundo porque se llama desde contexto global, donde fullname es A Name
//  El tercero porque accede a fullname de obj
var fullname = 'A Name';
var obj = {
  fullname: 'Full Name',
  prop: {
    fullname: 'Le Name',
    getFullname: function() {
      return this.fullname;
    }
  }
};
console.log(obj.prop.getFullname());
var aCall = obj.prop.getFullname;
console.log(aCall());
console.log(obj.fullname);
*/

/*
// Salida: 1, a pesar que se cambia el valor de a para 10, las funciones reciben hoisting y tienen prioridad sobre las variables
//  esto hace que a sea la funcion dentro de b y su valor jamas es 10. Al imprimir a, usa el valor del scope externo (1)
var a = 1;
function b() {
  a = 10;
  return;
  function a() {}
}
b();
console.log(a);
*/

/*
  Crea una clase persona que recibe un nombre, apellido y edad
  Agrega métodos para cambiar y obtener cada una de las propiedades
  Agrega un método (describe) que regrese una cadena como esta:
  {Nombre} {Apellido}, {edad} años.
*/
class Person {
  constructor(name, lastname, age) {
    this.name = name;
    this.lastname = lastname;
    this.age = age;
  }

  setName(name) {
    this.name = name;
  }

  getName() {
    return this.name;
  }

  setLastname(lastname) {
    this.lastname = lastname;
  }

  getLastname() {
    return this.lastname;
  }

  setAge(age) {
    this.age = age;
  }

  getAge() {
    return this.age;
  }

  describe() {
    return `${this.name} ${this.lastname}, ${this.age} años.`;
  }
}

exercises.push({
  description: 'Clase persona',
  execute: () => {
    const diego = new Person('Diego', 'Galindez', 25);
    console.log(diego.describe());
    console.log('Lastname: ', diego.getLastname());
    diego.setAge(21);
    diego.setLastname('');
    console.log('After change:', diego.describe());
    return 'Done!';
  }
})

/*
  Crea una clase Product que recibe un nombre y un valor
  tiene un método (value) que regresa su valor con iva (x1.16)

  Crea otra clase Cart que recibe un arreglo de Product
  tiene un método (add) que recibe un Product y lo agrega a su lista
  tiene un método (total) que regresa el total de todos los Product(con iva)

  Crea otra clase Food que hereda de product
  su método value regresa el valor sin iva
  Esta clase debería poder usarse en Cart
*/

class Product {
  constructor(name, cost) {
    this.name = name;
    this.cost = cost;
  }

  value() {
    return this.cost * 1.16;
  }
}

class Food extends Product {
  constructor(name, cost) {
    super(name, cost);
  }

  value() {
    return this.cost;
  }
}

class Cart {
  constructor(products) {
    this.products = products;
  }

  add(product) {
    this.products.push(product);
  }

  total() {
    return this.products.reduce((a, b) => a + b.value(), 0);
  }
}

exercises.push({
  description: 'Clases Product, Cart y Food',
  execute: () => {
    const cart = new Cart([
      new Product('Soap', 10),
      new Product('Razor', 40),
      new Product('Toothpaste', 50),
    ]);
    console.log(cart.total());
    cart.add(new Product('Something', 10));
    console.log('Adding 10:', cart.total());
    cart.add(new Food('Apple', 20));
    console.log('Adding 20 with Food:', cart.total());
    cart.add(new Food('Orange', 30));
    console.log('Adding 30 with Food:', cart.total());
    return 'Done!';
  }
})