const numberSingleInput = { type: 'number', times: 1 };
const textSingleInput = { type: 'text', times: 1 };

const numberTimesInput = (times) => ({ type: 'number', times });
const textTimesInput = (times) => ({ type: 'text', times });

const numberArrayInput = { type: 'number', condition: (v) => v >= 0, initial: 1 };
const textArrayInput = { type: 'text', condition: (v) => v };

/**
 * Creates prompts until given an empty value or just once if specified
 *
 * @param {string} type - Type of input, 'text' or 'number'
 * @param {(number\|string)} inital - Initial value passed to condition
 * @param {function} [condition] - Receives value to perform the condition on, returns true by default
 * @param {number} [times=Infinity] - Amount of times to prompt
*/
function ciclePrompt({type, condition = () => true, initial, times = Infinity}) {
  const input = () => prompt(`Give me ${type}`);
  const inputs = [];
  let value = initial;
  while (times && condition(value)) {
    times--;
    value = input();
    if (condition(value)) {
      type === 'number' ? inputs.push(Number(value)) : inputs.push(value);
    }
  }
  return inputs;
}

/**
 * Creates prompts until times is up or condition returns false
 *
 * @param {string} [type='text'] - Type of input, 'text' or 'number'
 * @param {function} [condition] - Receives value to perform the condition on
 * @param {(number\|string)} [inital] - Initial value passed to condition
 * @param {number} [times] - Amount of times to prompt, if falsy, prompts until condition returns false
*/
function createPrompts({type = 'text', condition, initial = 'initial', times}) {
  if (times) {
    const inputs = ciclePrompt({ type, times });
    return times === 1 ? inputs[0] : inputs;
  }
  return ciclePrompt({ type, condition, initial })
}

/**
 * @typedef Exercise
 * @property {string} description - The exercise requirements
 * @property {function} input - Function to be called as input to execute
 * @property {function} execute - The actual function
 * @property {boolean} noOutput - If present, does not create an alert
 */

/**
 * Creates a block for an exercise
 *
 * @param {Exercise} exercise - The exercise itself
*/
function createBlock({description, input, execute, noOutput}) {
  const wrapper = document.createElement('div');
  const text = document.createElement('div');
  const runButton = document.createElement('input');

  wrapper.setAttribute('class', 'exercise');
  text.setAttribute('class', 'text');
  text.innerHTML = description;

  runButton.setAttribute('type', 'button');
  runButton.setAttribute('value', 'Run');
  if (noOutput) {
    runButton.addEventListener('click', () => execute(input ? input() : null));
  } else {
    runButton.addEventListener('click', () => alert(execute(input ? input() : null)));
  }

  wrapper.appendChild(text);
  wrapper.appendChild(runButton);
  document.body.append(wrapper)
}

window.onload = () => exercises.forEach(createBlock);